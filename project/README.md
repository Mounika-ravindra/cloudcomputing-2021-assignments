# Cloud Computing 2021

## Second project

### Follwing is working: 

* Images & Info service is working with etag and expires
* DB access is direct without secrets to CloudSQL 
    (There are no secrets it will directly connect to the db)
* Infos & Images get published to DockerHub
* Ingress is working
* Runs on locally on Minikube

### What we still wanted todo
* Fix unneccessary env files now everywhere because of debugging
* Deploy to GKE
* Connect using secrets, what we have now is ugly :(

## Steps to run

# @Person Correctin: We need to whitelist your IP, pls contact us to get it whitelisted!

* ./build.sh
* ./deploy.sh
* ./shutdown.sh

