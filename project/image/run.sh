#!/bin/bash

export FLASK_APP=server
export FLASK_DEBUG=0
export FLASK_ENV=development

flask run --port=1070