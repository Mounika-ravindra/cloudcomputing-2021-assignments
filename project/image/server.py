from flask import Flask, request, Response, abort, make_response
import requests
import os
from datetime import datetime, date, timedelta
from functools import wraps

server = Flask(__name__)

S3_URL_FORMAT = os.environ.get('S3_URL_FORMAT', 'https://s3-eu-west-1.amazonaws.com/cloudcomputing-2018/project1/images/')
DEFAULT_UNUSED = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
EXPIRES = 60

def etag_cache(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = datetime.now()

        gen = func(*args, **kwargs)
        request_headers = request.headers
        previous_etag = request.headers.get('If-None-Match', '')
        gen.add_etag()
        current_etag = gen.headers.get('ETag')

        # Client has most recent data skip
        if current_etag == previous_etag:
            return make_response('', 304)

        expiration = timedelta(minutes=EXPIRES) + start
        gen.headers['Cache-Control'] = 'public, max-age=3600'
        gen.headers['Expires'] = expiration.strftime("%a, %d %b %Y %H:%M:%S GMT")
        return gen
    return wrapper

@server.route('/image/v1/watch/<string:sku>')
@etag_cache
def fetch_image (sku):

    url=S3_URL_FORMAT + sku + '.png'
    # Forward all request headers except host
    request_headers={key: value for (key, value) in request.headers}
    del request_headers['Host']

    s3_response = requests.get(url=url,headers=request_headers)
    s3_body = s3_response.content
    s3_status = s3_response.status_code
    response_headers = [(name, value) for (name, value) in s3_response.raw.headers.items()
               if name.lower() not in DEFAULT_UNUSED]

    client_response = Response(s3_body, s3_status, response_headers)

    if s3_status == 404: 
        return 'Not found', 404

    return client_response
