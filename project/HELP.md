# Minikube
https://gist.github.com/grugnog/caa118205ad498423266f26150a5d555


Problem: Minikube not starting anymore

rm -R ~/.Minikube

minikube start
minikube delete
minikube dashboard



kubectl apply -f "all.yml"

kubectl expose deployment infos-v1-service --type=NodePort --port=1080

# Docker 

docker login --username=yourhubusername 

> Create repositories 
> jogug/cc_infos_v1
> jogug/cc_images_v1

> Build and Tag local images


# GCloud
https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl
gcloud init
gcloud init --console-only


gcloud container clusters get-credentials cluster-1 --region europe-west1 --project cc-2021-312211