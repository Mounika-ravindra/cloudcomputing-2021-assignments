#!/bin/bash

# publish the image to the repository
docker push jogug/cc_infos_v1
docker push jogug/cc_images_v1

# apply
kubectl apply -f all.yml

# rollout changes
kubectl rollout restart deployment infos-v1
kubectl rollout restart deployment images-v1
