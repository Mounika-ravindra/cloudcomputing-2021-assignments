from info.models.Watch import Watch
from flask import Response, request
from info.shared.Db import db
import json
import sys

def test():
    watches = []
    for watch in Watch.query.limit(10).all():
        watches.append(watch.as_dict())
    return Response(json.dumps(watches), mimetype='application/json'), 200

def watch_post():
    body = request.get_json(force=True)
    new_watch = Watch()
    try:
        new_watch.take_dict(body)
        db.session.add(new_watch)
        db.session.commit()
        return "Watch added", 200
    except:
        return "Invalid Input", 400

def watch_get(sku):
    try:
        return Response(json.dumps(Watch.query.get(sku).as_dict()), mimetype='application/json'), 200
    except:
        return "Watch not found", 404

def watch_put(sku):
    try:
        update_watch = Watch.query.get(sku)
    except:
        return "Watch not found", 404

    body = request.get_json(force=True)
    try:
        update_watch.take_dict(body)
        db.session.commit()
        return "Successful operation", 200
    except:
        return "Invalid Input", 404

def watch_delete(sku):
    try:
        delete_watch = Watch.query.get(sku)
    except:
        return "Watch not found", 404
    
    try:
        db.session.delete(delete_watch)
        db.session.commit()
        return "Successful operation", 200
    except:
        return "Unsuccessful operation", 400

def watch_find_sku(prefix):
    skus = []
    for watch in Watch.query.filter(Watch.sku.ilike(prefix + '%')).all():
        skus.append(watch.sku)
    return Response(json.dumps(skus), mimetype='application/json'), 200

def watch_find():
    watches = []
    for watch in Watch.query.filter(
            Watch.sku.ilike('%' + request.args['sku'] + '%'),
            Watch.type.ilike(request.args['type']),
            Watch.status.ilike(request.args['status']),
            Watch.gender.ilike(request.args['gender']),
            Watch.year.ilike(int(request.args['year'])),
            ).all():
        watches.append(watch.as_dict())
    if len(watches) == 0:
        return "Watch not found", 404
    else:
        return Response(json.dumps(watches), mimetype='application/json'), 200