import os
from flask import Flask
from flask_basicauth import BasicAuth
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

from info.controllers import WatchController, ErrorController
from info.shared.Db import db
from info.models.Watch import Watch

# Load Env Variables
db_user = os.environ.get('DB_USER')
db_pass = os.environ.get('DB_PASS')
db_dbname = os.environ.get('DB_DBNAME')
db_host = os.environ.get('DB_HOST')
db_port = os.environ.get('DB_PORT')

server = Flask(__name__)
CORS(server, supports_credentials=True)
server.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+db_user+':'+db_pass+'@'+db_host+':'+db_port+'/'+db_dbname
server.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
server.config['SQLALCHEMY_RECORD_QUERIES'] = False
server.config['SQLALCHEMY_ECHO'] = False

# Enabled basic auth
#---------------------
server.config['BASIC_AUTH_USERNAME'] = os.environ.get('HTTP_USER')
server.config['BASIC_AUTH_PASSWORD'] = os.environ.get('HTTP_PASS')
server.config['BASIC_AUTH_FORCE'] = os.environ.get('BASIC_AUTH_FORCE') == '1'
basic_auth = BasicAuth(server)
db.init_app(server)

# Routing
# End points defined in controllers folder/module
server.add_url_rule('/', methods=["GET"], 
    view_func=WatchController.test, defaults={})

server.add_url_rule('/info/v1/watch', methods=["POST"], 
    view_func=WatchController.watch_post, defaults={})
server.add_url_rule('/info/v1/watch/<sku>', methods=["GET"], 
    view_func=WatchController.watch_get, defaults={})
server.add_url_rule('/info/v1/watch/<sku>', methods=["PUT"], 
    view_func=WatchController.watch_put, defaults={})
server.add_url_rule('/info/v1/watch/<sku>', methods=["DELETE"], 
    view_func=WatchController.watch_delete, defaults={})

server.add_url_rule('/info/v1/watch/complete-sku/<prefix>', methods=["GET"], 
    view_func=WatchController.watch_find_sku, defaults={})

server.add_url_rule('/info/v1/watch/find', methods=["GET"], 
    view_func=WatchController.watch_find, defaults={})

server.register_error_handler(404, ErrorController.page_not_found)