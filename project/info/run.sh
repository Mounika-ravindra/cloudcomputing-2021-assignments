#!/bin/bash
export DB_HOST=127.0.0.1
export DB_PORT=3306
export DB_DBNAME=watches
export DB_USER=watches
export DB_PASS=watches

export HTTP_USER=cloud
export HTTP_PASS=computing

export FLASK_APP=server
export FLASK_DEBUG=0
export FLASK_ENV=development

export BASIC_AUTH_FORCE=0
flask run --port=1080