# Cloud Computing 2021

## First project

### MySql setup

##### Start API Server
```
docker-compose up -d
```

The server should now be accessible from http://localhost:1080/. Swagger Editor does not support basic auth thus its turned off by default.

##### Stop API Server
```
docker-compose down
```

## Helpful Links
https://vsupalov.com/docker-arg-env-variable-guide/
