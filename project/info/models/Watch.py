from info.shared.Db import db

class Watch(db.Model):
    __tablename__ = "watches"
 
    sku = db.Column(db.String, primary_key=True)
    type = db.Column(db.String)  
    status = db.Column(db.String)
    gender = db.Column(db.String)
    year = db.Column(db.Integer)
    dial_material = db.Column(db.String)
    dial_color = db.Column(db.String)
    case_material = db.Column(db.String)
    case_form = db.Column(db.String)
    bracelet_material = db.Column(db.String)
    movement = db.Column(db.String)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def take_dict(self, values):
        for name, value in values.items():
            setattr(self, name, value)
        setattr(self, 'year', int(values['year']))
        
    def __repr__(self):
        return 'Watch sku: '+ self.sku +', ' + self.type + '<br>'