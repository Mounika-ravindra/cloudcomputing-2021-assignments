#!/bin/bash
kubectl delete deployment images-v1
kubectl delete deployment infos-v1
kubectl delete service images-v1-service
kubectl delete service infos-v1-service
kubectl delete configmap info-configmap
kubectl delete ingress ingress-load