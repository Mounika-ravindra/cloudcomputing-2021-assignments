# AWS Lambda Info v2

## Public endpoint

Get
> https://z6g084d4ui.execute-api.eu-west-1.amazonaws.com/deployment/watch/WAT2351.BB0957

Post eg with httpie:
> http -v POST https://z6g084d4ui.execute-api.eu-west-1.amazonaws.com/deployment/watch sku=JOEL2 type=watch status=old year=1990 gender=man

## Requirements
Please configure your aws-cli  
The key and access token can be generated in aws console.
```
 aws configure
```
We use jq to parse piped responses. To install on ubuntu execute:
```
 sudo apt install jq
```

We use only a single dependency boto3. Please install using:
```
 pip3 install boto3
```

## Initializin DynamoDB, Lambda Creation, API Gateway Creation
Please specify your region and account id in the credentials.sh  
watches_dynamo_lambda_cc is the role_name that will be created as  
the IAM role to be attached to the lambda functions.  

Execute to initialize DynamoDB and fill it with watches.json: 
```
 ./initialize_dynamodb.sh
```

Execute to create the lambda functions: 
```
 ./create_lambdas.sh
```

Followed by to create the gateway: 
```
 ./create_api_gateway.sh
```

To update the lambda function you can use: 
```
 ./update_lambdas.sh
```