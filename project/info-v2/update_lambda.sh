zip lambda_watches.zip *.py
# update lambda function on each code change
aws lambda update-function-code --function-name get_watch --zip-file fileb://lambda_watches.zip --publish
aws lambda update-function-code --function-name post_watch --zip-file fileb://lambda_watches.zip --publish