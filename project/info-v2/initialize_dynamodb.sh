#Delete table if it already exists
aws dynamodb delete-table --table-name watches

# wait until table is deleted
aws dynamodb wait table-not-exists --table-name watches

# Create watches table in dynamodb
aws dynamodb create-table \
    --table-name watches \
    --attribute-definitions \
        AttributeName=sku,AttributeType=S \
    --key-schema \
        AttributeName=sku,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1000

# wait until table is created 
aws dynamodb wait table-exists --table-name watches

# run python file to update data into watches table
python3 load_data_dynamodb.py

aws dynamodb update-table \
--provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 --table-name watches