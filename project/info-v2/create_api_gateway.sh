. ./credentials.sh

#  create APIGateway with OPENAPI specification
apigateway_id=$(aws apigateway import-rest-api --cli-binary-format raw-in-base64-out --body file://info_openapi_v2.yaml --no-fail-on-warnings | jq -r .id)

echo 'Get API ID'
echo $apigateway_id

echo 'Create Gatway Role'
role_name=watches_gateway_cc
rolearn=$(aws iam create-role --role-name ${role_name} --assume-role-policy-document file://role-policy-document.json | jq -r .Role.Arn)

echo 'Waiting 5'
sleep 5
aws iam wait role-exists --role-name $role_name

aws iam attach-role-policy \
    --role-name $role_name \
    --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaRole

echo 'Waiting 5'
sleep 5
aws iam wait role-exists --role-name $role_name

# get resource ids to add permissions
resource_id_get=$(aws apigateway get-resources --rest-api-id $apigateway_id | jq -r '(.items[] | select(.path == "/watch/{sku}")).id')
resource_id_post=$(aws apigateway get-resources --rest-api-id $apigateway_id | jq -r '(.items[] | select(.path == "/watch")).id')

echo 'Get Res get ID'
echo $resource_id_get

echo 'Get Res post ID'
echo $resource_id_post

# arn of the actual lambda function
watch_get_function=arn:aws:apigateway:$region:lambda:path/2015-03-31/functions/arn:aws:lambda:$region:$account:function:get_watch/invocations
echo 'Uri get'
echo $watch_get_function
aws apigateway put-integration \
 --rest-api-id $apigateway_id \
 --resource-id $resource_id_get \
 --http-method GET --type AWS_PROXY \
 --integration-http-method POST \
 --credentials $rolearn \
 --uri $watch_get_function


watch_post_function=arn:aws:apigateway:$region:lambda:path/2015-03-31/functions/arn:aws:lambda:$region:$account:function:post_watch/invocations
echo 'Get Uri post'
echo $watch_post_function
aws apigateway put-integration \
--rest-api-id $apigateway_id \
--resource-id $resource_id_post \
--http-method POST --type AWS_PROXY \
--integration-http-method POST \
--credentials $rolearn \
--uri $watch_post_function

aws lambda add-permission \
--function-name get_watch \
--statement-id apigateway-lambda-policy-$apigateway_id \
--action lambda:InvokeFunction --principal apigateway.amazonaws.com \
--source-arn "arn:aws:execute-api:${region}:${account}:${apigateway_id}/*/GET/watch/{sku}"
aws lambda add-permission \
--function-name post_watch \
--statement-id apigateway-lambda-policy-$apigateway_id \
--action lambda:InvokeFunction --principal apigateway.amazonaws.com \
--source-arn "arn:aws:execute-api:${region}:${account}:${apigateway_id}/*/POST/watch"

echo 'Deploying api prefix deployment'
aws apigateway create-deployment --rest-api-id $apigateway_id --stage-name deployment
