import boto3

# Get the service resource.
dynamodb = boto3.resource('dynamodb')

table = table = dynamodb.create_table(
    TableName='watches',
    KeySchema=[
        {
            'AttributeName': 'sku',
            'KeyType': 'HASH'
        }
    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'sku',
            'AttributeType': 'S'
        }
    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
)

# Wait until the table exists.
table.meta.client.get_waiter('table_exists').wait(TableName='watches')
