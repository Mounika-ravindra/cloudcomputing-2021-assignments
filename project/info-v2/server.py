import boto3
import json

dynamo = boto3.resource("dynamodb")
table_watches = dynamo.Table("watches")

# Response Builder Types
RESPONSE_TYPE_PLAIN = 1
RESPONSE_TYPE_JSON = 2

# Http Codes
HTTP_NOT_FOUND = 404
HTTP_OK = 200
HTTP_BAD_REQUEST = 400

def get_watch(event, context):
    sku = event["pathParameters"]["sku"]

    stmt = table_watches.get_item(Key={"sku": sku})

    if "Item" not in stmt:
        return response_builder(
                headers = event["headers"], 
                status = HTTP_NOT_FOUND, 
                body = "Watch not found"
            )

    item = stmt["Item"]
    item["year"] = int(item["year"])

    return response_builder(
            RESPONSE_TYPE_JSON,
            headers = event["headers"], 
            status = HTTP_OK, 
            body = item
        )


def post_watch(event, context):
    # Catch wrong content-type if it is set wrong, else assume correct provided
    if event["headers"] is not None and "Content-Type" in event["headers"] and event["headers"]["Content-Type"] != "application/json":
        return response_builder(
                headers = event["headers"], 
                status = HTTP_BAD_REQUEST, 
                body = "Bad Request, Content-Type does not match!"
            )

    try:
        body = json.loads(event["body"])
    except json.JSONDecodeError as e:
        return response_builder(
                headers = event["headers"], 
                status = HTTP_BAD_REQUEST, 
                body = "Invalid input: JSON malformed!"
            )

    # Process required fields
    required_fields = { "sku", "type", "status", "gender", "year" }
    cleaned_body = {}
    for required_field in required_fields:
        if required_field not in body.keys():
            return response_builder(
                    headers = event["headers"], 
                    status = HTTP_BAD_REQUEST, 
                    body = "Invalid input: Missing required field: " + required_field
                )
        else:
            cleaned_body[required_field] = body[required_field]


    # Check if already exists
    try:
        stmt = table_watches.get_item(
            Key={
                "sku": body["sku"]
            }
        )
        if "Item" in stmt:
            return response_builder(
                    headers = event["headers"], 
                    status = HTTP_BAD_REQUEST, 
                    body = "Watch already exists " + body["sku"])
    except Exception as e:
        return response_builder(
                headers = event["headers"], 
                status = HTTP_BAD_REQUEST, 
                body = "Check for existence failed " + str(e))

    try:
        cleaned_body["year"] = int(body["year"])
    except ValueError:
        return response_builder(
                    headers = event["headers"], 
                    status = HTTP_BAD_REQUEST, 
                    body = "Invalid input: Year could not be parsed to int!"
                )

    if body["type"] not in {"watch", "chrono"}:
        return response_builder(
                headers = event["headers"], 
                status = HTTP_BAD_REQUEST, 
                body = "Invalid input: type must be watch or chrono!"
            )

    if body["status"] not in {"old", "current", "outlet"}:
        return response_builder(
                headers = event["headers"], 
                status = HTTP_BAD_REQUEST, 
                body = "Invalid input: status must be old, current or outlet!"
            )

    if body["gender"] not in {"man", "woman"}:
        return response_builder(
                headers = event["headers"], 
                status = HTTP_BAD_REQUEST, 
                body = "Invalid input: Gender must be man or woman!"
            )

    # Process optional fields
    optional_fields = {"dial_material", "dial_color", "case_material", "case_form", "bracelet_material", "movement"}
    for optional_field in optional_fields:
            if optional_field not in body.keys():
                cleaned_body[optional_field] = ""
            else:
                cleaned_body[optional_field] = body[optional_field]

    # Try inserting
    try:
        table_watches.put_item(Item=cleaned_body)
        return response_builder(
                headers = event["headers"], 
                status = HTTP_OK, 
                body = "Successful operation")
    except Exception as e:
        return response_builder(
                headers = event["headers"], 
                status = HTTP_BAD_REQUEST, 
                body = "Operation failed " + str(e))


def response_builder(
                    response_type = RESPONSE_TYPE_PLAIN,
                    headers = {}, 
                    status = HTTP_OK, 
                    body = ""):

    # Plain text response
    if response_type == RESPONSE_TYPE_PLAIN:

        temp_headers = {
            "Content-Type": "text/plain"
        }

    # Json response
    elif response_type == RESPONSE_TYPE_JSON:

        temp_headers = {
            "Content-Type": "application/json"
        }
        body = json.dumps(body)

    # Pass on allow origin
    if headers:
        lowercase = dict((key.lower(),value) for key,value in headers.items())
        if "origin" in lowercase.keys():
            temp_headers["Access-Control-Allow-Origin"] = lowercase["origin"]

    return {
        "statusCode": status,
        "headers": temp_headers,
        "body": body
    }