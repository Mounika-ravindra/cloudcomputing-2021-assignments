from decimal import Decimal
import json
import boto3

def put_watches(watches, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb', region_name="eu-central-1")

    table = dynamodb.Table('watches')
    for watch in watches:
        print("Loading ....")
        table.put_item(Item=watch)
    print("Done Loading watches")


if __name__ == '__main__':
    with open("watches.json") as json_file:
        watches_list = json.load(json_file, parse_float=Decimal)
    put_watches(watches_list)
