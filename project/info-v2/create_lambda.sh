. ./credentials.sh
zip lambda_watches.zip server.py

echo '1. Create a role'
role_name=watches_dynamo_lambda_cc
aws iam create-role --role-name $role_name \
--assume-role-policy-document file://role-policy-document.json

echo 'Waiting 5 sec'
sleep 5
aws iam wait role-exists --role-name $role_name

echo '2. Attaching policy to role'
aws iam attach-role-policy --role-name $role_name \
 --policy-arn arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs
aws iam attach-role-policy --role-name $role_name \
 --policy-arn arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess
aws iam attach-role-policy --role-name $role_name \
 --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

echo 'Waiting 15 sec'
sleep 15
aws iam wait role-exists --role-name $role_name

echo '3 Creating functions'
aws lambda create-function \
--function-name get_watch \
--runtime python3.8 \
--role arn:aws:iam::$account:role/$role_name \
--handler server.get_watch --zip-file fileb://lambda_watches.zip


aws lambda create-function \
--function-name post_watch \
--runtime python3.8 \
--role arn:aws:iam::$account:role/$role_name \
--handler server.post_watch --zip-file fileb://lambda_watches.zip
